Name:           sshd-config
Version:        0.1.1
Release:        1%{?dist}
Summary:        SSHD configuration that allows logging in via signed SSH certificates
License:        MIT
Source0:        %{name}-%{version}.tar.gz

%description
Configuration files for ssh server that allow logging in via signed SSH certificates
provided by mf's HashiCorp Vault instance.

%prep
%autosetup

%install
mkdir -p %{buildroot}%{_sysconfdir}/ssh/sshd_config.d

install -m 600 trusted-user-ca-keys.pem %{buildroot}%{_sysconfdir}/ssh/sshd_config.d/
install -m 600 90-mf-ca.conf %{buildroot}%{_sysconfdir}/ssh/sshd_config.d/

%post
if [ -d /run/systemd ]; then
  systemctl restart sshd
fi

%postun
if [ -d /run/systemd ]; then
  systemctl restart sshd
fi

%files
%config %{_sysconfdir}/ssh/sshd_config.d/{trusted-user-ca-keys.pem,90-mf-ca.conf}

%changelog
* Wed Aug 09 2023 Matej Focko <me@mfocko.xyz> - 0.1.1-1
- Renamed package

* Wed Aug 09 2023 Matej Focko <me@mfocko.xyz> - 0.1-1
- Initial release

