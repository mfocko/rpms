# mf's RPMs

Here you can find RPMs that you cannot casually find in the repositories.

## `sshd-config`

`sshd-config` is my own configuration for sshd that includes, for now, just the
setting of CA authority for SSH auth.

## `rspamd`

`rspamd` is a spam filter for mail server deployed on my VPS, there are no
up-to-date releases to Fedora though.
